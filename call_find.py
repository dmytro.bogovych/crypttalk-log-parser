#!/usr/bin/python3

import sys
import subprocess
import re
import zipfile
import operator
import tempfile
import argparse

from pathlib import Path

PRINT_ICE = False
PRINT_SIP = False
PRINT_ALL = False

def check_archive(path: Path):
    zip = zipfile.ZipFile(path)
    names = zip.namelist()
    lst = []

    for fn in names:
        if fn.startswith('logfile.') and fn.endswith('.html'):
            # Get index
            pattern = r"(?P<number>[\d]+)"
            m = re.search(pattern=pattern, string=fn)
            if m is not None:
                idx = int(m.group('number'))
            else:
                idx = 0
            lst.append((idx, fn))

    if len(lst) > 0:
        # Sort list
        lst.sort(key = operator.itemgetter(0), reverse=True)

        for t in lst:
            file_idx = t[0]
            file_name = t[1]

            file_content = zip.read(file_name)
            # Write it into temporary file
            temp_name = tempfile.mktemp() 
            with open(temp_name, 'wb') as tf:
                tf.write(file_content)
            
            print(f'Checking file {file_name} with temp file {temp_name}...')
            check_html(temp_name)

def check_html(path: Path):
    # Process log line by line
    with open(path, 'rt') as f:
        for line in f:
            check_log_line(line)

# <tr><td class="active"></td><td class="info top"><pre>[110462]  2023.06.19 18:41:30.793   Info                        Core_EndPoint.cpp  4979  </pre></td><td class="info"><pre class="message-content">Connectivity checks success.</pre></td></tr>

def get_pre(html_text: str) -> str:
    # regex to extract required strings
    reg_str = '<pre.*?>(.*?)</pre>'
    res = re.findall(reg_str, html_text)
    if reg_str:    
        return ''.join(res)
    else:
        return ''
 
PRE_MESSAGE_CONTENT = '<pre class="message-content">'

ONE_LINE_EVENTS = [
    'OnIncomingCall',
    'OnCallProvisional', 
    'OnCallEstablished event', 
    'Starting connectivity checks', 
    'OnEncryptionRequest event',
    'OnEncryptionStart event', 
    'Connectivity checks',
    'OnReceiverReport', 
    'OnCallTerminated event',
    'OnFirstRTP',
    'Check list is completed',
    'Received:',
    'OnEncryptionHandshake',
    'OnEncryptionStart'
]

IGNORE_WORDS = [
    'CallStateMachine'
]

def check_log_line(html_line: str):
    has_tag = '<' in html_line and '>' in html_line

    has_packet = '&lt;---' in html_line or '---&gt;' in html_line
    if '<td class="info">' in html_line or has_packet or (not has_tag and PRINT_ALL):
        has_ice = 'ICE' in html_line

        if has_tag:
            line = get_pre(html_line)
        else:
            line = html_line.strip()

        if has_ice:
            # Because of ICE related lines are not uniformly terminated
            pre_pos = html_line.find(PRE_MESSAGE_CONTENT)
            if pre_pos > 0:
                ice_content = html_line[pre_pos + len(PRE_MESSAGE_CONTENT):]
                line = f'{line} {(ice_content.strip())}'
        
        if has_packet:
            line = html_line.replace('&lt;---', '<---').replace('---&gt;', '--->').strip()

        if PRINT_ALL:
            print(line)
        else:
            if has_ice and PRINT_ICE:
                print(line)

            if has_packet and PRINT_SIP:
                print(line)
            
            for s in IGNORE_WORDS:
                if s in line:
                    return

            if 'Attempt to establish call' in line:
                print()
                print(line)
            elif 'Received:' in line:
                print(line)
                print()
            else:
                for s in ONE_LINE_EVENTS:
                    if s in line:
                        print(line)
                        break
  
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(prog='CT log parser', description='Produces digest for CT logs')
    parser.add_argument('-c', '--ice', action='store_true', help='print ICE log')
    parser.add_argument('-s', '--sip', action='store_true', help='print SIP signalling')
    parser.add_argument('-f', '--full', action='store_true', help='print all')
    parser.add_argument('log', help='path to .zip or .html file')

    if len(sys.argv) == 1:
        parser.print_help()
        exit(0)

    args = parser.parse_args()

    p = Path(args.log).resolve()
    PRINT_ICE = args.ice
    PRINT_SIP = args.sip
    PRINT_ALL = args.full

    if p.suffix == '.zip':
        check_archive(p)
    elif p.suffix == '.html':
        check_html(p)
    else:
        print('This format type is not supported.')            
            